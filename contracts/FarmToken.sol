//SPDX-License-Identifier
pragma solidity >=0.4.22 < 0.9.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract FarmToken is ERC20 {
    string public name = "IncentiveToken";
    string public description = "Incentives for Tokenized Farm Asset";
    string public symbol = "IncentiCoin";
    uint8 public decimals = 2;
    uint256 public INITIAL_SUPPLY = 10000000;
    uint256 public tokenRate = 10;

    constructor() public {
        _mint(msg.sender, INITIAL_SUPPLY);
    }

    function transferTokensForFarmAsset(address to, uint256 farmAssetProduced, string memory productName) public returns (bool) {
        require(to != address(0), "Invalid address");
        require(farmAssetProduced > 0, "Farm asset produced must be greater than 0");

        uint256 tokensToTransfer = farmAssetProduced * tokenRate;
        require(tokensToTransfer <= balanceOf(msg.sender), "Insufficient balance");

        _transfer(msg.sender, to, tokensToTransfer);

        return true;
    }
}